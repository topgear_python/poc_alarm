import unittest
from main import addAlaram, updateAlaram, deleteAlaram, deleteAll, alaramCount, alaramDisplay


class testAlaram(unittest.TestCase):
    def setUp(self):
        pass
    #a) Alaram is adding
    def testaddAlaram1(self):
        self.assertEqual(addAlaram(1, "first alaram", "10:30", True), 1)

    # a) Alaram is adding
    def testaddAlaram2(self):
        self.assertEqual(addAlaram(2, "Second alaram", "11:30", True), 1)

    # b) Alaram is updating
    def testupdatealaram1(self):
        self.assertEqual(updateAlaram(2, "Second alaram", "12:30", True), 0)

    # c) Alaram is deleting
    def testremovealaram(self):
        self.assertEqual(deleteAlaram(1), 0)

    # d) All Alarams are deleted
    def testdeleteAll(self):
        self.assertEqual(deleteAll(), 0)

    # a) Alaram is added
    def testaddAlaram3(self):
        self.assertEqual(addAlaram(3, "Third alaram", "11:30", True), 1)

    # e) All Alarams are counted and listed
    def testcountalaram(self):
        self.assertEqual(alaramCount(), 3)#We created 3 alarams

    # f) Alaram is displaying
    def testalaramdisplay(self):
        self.assertEqual(alaramDisplay(3), None)


if __name__ == '__main__':
    unittest.main()
